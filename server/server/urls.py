from django.urls import path
from server.views import index, get_julia, get_all_angles


urlpatterns = (
    path('', index),
    path('api/get_all_angles', get_all_angles),
    path('api/<str:angle>', get_julia)
)
