from django.db import models


class Julia(models.Model):

    class Meta:
        db_table = 'julias'

    set_id = models.AutoField(primary_key=True)
    angle = models.SmallIntegerField(unique=True, default=0)
    set_image = models.TextField()
