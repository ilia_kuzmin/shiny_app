import base64

from PIL import Image
from io import BytesIO
from math import sin, cos


def julia_set(angle):
    maximum_iterations = 128
    domain = {'x': {'left': -1.5,
                    'right': 1.5
                    },
              'y': {'left': -1.5,
                    'right': 1.5
                    }
              }
    size_of_image = {'x': 1024,
                     'y': 1024}
    c = complex(0.32 * sin(angle), 0.32 * (1 - cos(angle)))
    image = Image.new("RGB", (size_of_image.get('x'), size_of_image.get('y')))

    for y in range(size_of_image.get('y')):
        z_y = y * (domain.get('y').get('right') - domain.get('y').get('left')) / (size_of_image.get('y') - 1) + \
              domain.get('y').get('left')

        for x in range(size_of_image.get('y')):
            z_x = x * (domain.get('x').get('right') - domain.get('x').get('left')) / (size_of_image.get('x') - 1) + \
                  domain.get('x').get('left')

            z = complex(z_x, z_y)

            for i in range(maximum_iterations):

                if abs(z) > 2.0:
                    break

                z = z * z + c
                r = i % 4 * 64
                g = i % 8 * 0
                b = i % 16 * 16
                image.putpixel((x, y), b * 65536 + g * 256 + r)

    buffered = BytesIO()
    image.save(buffered, format="PNG")
    img_str = base64.b64encode(buffered.getvalue())

    return img_str.decode('utf-8')
