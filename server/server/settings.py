import os
import dj_database_url
from django.conf.global_settings import DATABASES

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

SECRET_KEY = os.environ.get('SECRET_KEY', os.urandom(32))

DEBUG = os.environ.get('DEBUG', 'true') == 'true'

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', 'localhost,127.0.0.1').split(',')

MIDDLEWARE = ['django.middleware.common.CommonMiddleware',
              'django.middleware.csrf.CsrfViewMiddleware',
              'django.middleware.clickjacking.XFrameOptionsMiddleware']

ROOT_URLCONF = 'server.urls'

DATABASES['default'] = dj_database_url.config()

INSTALLED_APPS = [
    'server.apps.ServerConfig',
]
