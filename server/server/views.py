from django.http import HttpResponse
from django.db import connection
from server.models import Julia
from math import pi


def index(request):
    return HttpResponse('It is working!')


def get_julia(request, angle):
    angle = int(round(float(angle) * 100))

    if 'julias' not in connection.introspection.table_names():
        return HttpResponse("Sorry, data was lost", status=400)

    if int(round(pi * 100)) <= angle <= 0 or angle % 5 != 0:
        return HttpResponse(f"This value - {angle} - of an angle is not supported", status=400)

    try:
        julia = Julia.objects.get(angle=angle)
    except Julia.DoesNotExist:
        return HttpResponse("Sorry, data was lost", status=400)

    return HttpResponse("data:image/png;base64," + julia.set_image)


def get_all_angles(request):
    return HttpResponse(' '.join([str(angle / 100) for angle in Julia.objects.values_list('angle', flat=True)]))
