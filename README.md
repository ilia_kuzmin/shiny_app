# About
R Shiny + Django project to display pictures with Julia set which a loaded from server on Heroku.  
# How to install
1. Clone the project
2. To download required packages for client run the commands from terminal:  
    + Rscript -e 'install.packages("shiny")'
    + Rscript -e 'install.packages("httr")'
    + Rscript -e 'install.packages("jsonlite")'
3. Download required packages for server (from server directory):  
    + pip3 install -r requirements.txt
# How to use
1. To launch the server locally:   
   Run Postgres server and create database for pictures   
   Then run commands (from server directory):  
    + export DATABASE_URL=postgres://<i></i>username:password@localhost/db_name  
      Where you need to set your username, password and datatabase name
    + python3 manage.py migarate
    + python3 manage.py loaddata database_dump/dumped_data.json
    + python3 manage.py runserver
2. To laucnh client run (from client directory):  
  + Rscript -e 'shiny::runApp("app.R", launch.browser = TRUE)' arg  
    Where arg is local or heroku (default is heroku) 
